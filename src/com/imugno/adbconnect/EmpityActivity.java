package com.imugno.adbconnect;

import android.app.Activity;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

public class EmpityActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Context context = getApplicationContext();
		
		Common.init(context);
		Common.on=!Common.on;
		Common.setNoti(context);
		Common.setAdb(context);
		
		// aggiorno il widget (ammesso che ci sia)
		Intent intent = new Intent(this, wid_class.class);
		intent.setAction("android.appwidget.action.APPWIDGET_UPDATE");
		int ids[] = AppWidgetManager.getInstance(getApplication()).getAppWidgetIds(new ComponentName(getApplication(), wid_class.class));
		intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS,ids);
		sendBroadcast(intent);
		
		finish();
		super.onCreate(savedInstanceState);
	}
}
