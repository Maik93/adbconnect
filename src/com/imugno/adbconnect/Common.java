/*
 * Metodi in comune tra Activity e Widget
 */

package com.imugno.adbconnect;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import android.app.Notification;
import android.app.Notification.Builder;
import android.app.NotificationManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.widget.Toast;

import com.stericson.RootTools.RootTools;
import com.stericson.RootTools.exceptions.RootDeniedException;
import com.stericson.RootTools.execution.CommandCapture;

public class Common {

	public static boolean on = false;
	public static final int notID = 1; // dovrebbe esser la dichiarazione di una costante, se non sbaglio
	public static Builder noti;
	
	public static void init(Context context){
		Bitmap bm = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_launcher);
		if(noti==null) noti = new Notification.Builder(context) // richiede API level 11
		   .setContentTitle("adb attivo")
		   .setContentText("connettiti dal pc tramite il tuo IP")
		   .setSmallIcon(R.drawable.ic_stat_adb)
		   .setLargeIcon(bm)
		   .setOngoing(true) // non rimovibile
		   .setShowWhen(false) // non mostra la data di attivazione sulla destra
		   //.setProgress(0, 0, true) // la progressbar infinita
		   .setTicker("connessione adb attiva") //testo che appare quando parte la notifica
		   .setPriority(Notification.PRIORITY_HIGH); //cos� � sempre la prima da sinistra
	}
	
	public static void setNoti(Context context){
		NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		if(on){
			WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
			if(wifiManager.getConnectionInfo().getIpAddress()==0){
				noti//.setProgress(0, 0, false) // rimuovo la barra
					.setContentText("Devi essere connesso ad una rete locale!");
			} else noti.setContentText("connettiti dal tuo pc a "+getIpAddr(context));
			mNotificationManager.notify(notID, noti.build());
		}
		else mNotificationManager.cancel(notID);
	}

	private static String getIpAddr(Context context) {
		WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
		WifiInfo wifiInfo = wifiManager.getConnectionInfo();
		int ip = wifiInfo.getIpAddress();
		
		String ipString = String.format(
				"%d.%d.%d.%d",
				(ip & 0xff),
				(ip >> 8 & 0xff),
				(ip >> 16 & 0xff),
				(ip >> 24 & 0xff));
		
		return ipString;
	}

	public static void setAdb(Context context){
		if (RootTools.isBusyboxAvailable()) { // guarda se c'� busybox (non so se serva davvero)
			if (RootTools.isAccessGiven()) { // controlla tu abbia il root (e questo serve peddavvero)
				CommandCapture command; // mposta il comando per accendere-spengere la modalit� wifi di adb 
				if(on) command = new CommandCapture(0,"setprop service.adb.tcp.port 5555","stop adbd","start adbd","setprop service.adb.tcp.port -1");
				else command = new CommandCapture(0,"stop adbd","start adbd");
				// esegue il comando (senza sto tritello da errore....)
				try {RootTools.getShell(true).add(command);}catch (IOException e) {e.printStackTrace();} catch (TimeoutException e) {e.printStackTrace();} catch (RootDeniedException e) {e.printStackTrace();}
			} else Toast.makeText(context, "Barbone, devi fargli il root a sto telefono!!\nSenn� � inutile....", Toast.LENGTH_LONG).show();
		} else Toast.makeText(context, "A quanto pare non hai BusyBox!\nNon fare il barbone, scaricalo!!", Toast.LENGTH_LONG).show();
	}
}
