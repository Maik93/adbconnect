package com.imugno.adbconnect;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;


public class wid_class extends AppWidgetProvider {
	
	private static String WIDGET_BUTTON = "widget_premuto";
	private static RemoteViews views;
	
	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds){
		super.onUpdate(context, appWidgetManager, appWidgetIds);
		
		Intent intent = new Intent(context, wid_class.class);
		intent.setAction(WIDGET_BUTTON);
		PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0);
		if(views==null) views = new RemoteViews(context.getPackageName(), R.layout.wid_layout);
		views.setOnClickPendingIntent(R.id.img, pendingIntent);
		
		Common.init(context);
		setImg();
		
		appWidgetManager.updateAppWidget(appWidgetIds, views);
	}
	
	@Override
	public void onReceive(Context context, Intent intent){
		if((intent.getAction().equals(WIDGET_BUTTON))){
			AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
			int[] appWidgetIds = appWidgetManager.getAppWidgetIds(new ComponentName(context,wid_class.class));

			if(views==null) views = new RemoteViews(context.getPackageName(), R.layout.wid_layout);
			Common.init(context);
			
			Common.on=!Common.on;
			setImg();
			Common.setNoti(context);
			Common.setAdb(context);
			
			appWidgetManager.updateAppWidget(appWidgetIds, views);
		} else super.onReceive(context, intent);
	}

	private void setImg(){
		if(Common.on) views.setImageViewResource(R.id.img,R.drawable.ic_on);
		else views.setImageViewResource(R.id.img,R.drawable.ic_off);
	}
}
